---

linux_installed:
  pkg.installed:
    - pkgs:
        - kernel
  cmd.run:
    - name: 'dracut --force'

grub_updated:
  file.managed:
    - name: '/etc/default/grub'
    - source: 'salt://files/grub_simple_conf'
  cmd.run:
    - name: 'grub2-mkconfig -o /boot/grub2.grub.cfg'
