#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]
  then echo 'Требуются права суперпользователя'
  exit 1
fi

source ./executor.sh

salt '*-blue' test.ping
salt -G 'os:CentOS' test.ping
salt -N blue test.ping
salt -C 'G@os:CentOS and *blue or S@192.168.122.112' test.ping
